package deloitte.academy.samples;

public class DowhileLoop {

	public static void main(String[] args) {
		int i= 1;
		do {
			// Print the statement
			System.out.println("Hello world");
			//update the expression
			i++;
		}
		// test expression
		while(i < 6);

	}

}
