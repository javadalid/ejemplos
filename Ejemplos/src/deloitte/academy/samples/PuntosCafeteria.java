package deloitte.academy.samples;

public class PuntosCafeteria {
	private int id, totalPuntos;
	private String nombre, tipo;

	public PuntosCafeteria(int id, int totalPuntos, String nombre, String tipo) {
		super();
		this.id = id;
		this.totalPuntos = totalPuntos;
		this.nombre = nombre;
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTotalPuntos() {
		return totalPuntos;
	}

	public void setTotalPuntos(int totalPuntos) {
		this.totalPuntos = totalPuntos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
