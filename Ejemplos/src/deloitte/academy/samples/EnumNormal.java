package deloitte.academy.samples;

enum Color{
	VERDE, ROJO, AZUL;
}

public class EnumNormal {

	public static void main(String[] args) {
		Color color = Color.VERDE;
		System.out.println(color);

	}

}
