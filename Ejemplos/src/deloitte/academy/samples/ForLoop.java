package deloitte.academy.samples;

/**
 * Comentario a nivel de clase
 * @author Javier Adalid 
 *
 */
public class ForLoop {

	/**
	 * Comentario para m�todos
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Comentario simple
		 */
		for (int i = 2; i <= 4; i++) {
			System.out.println("Value of i: "+i);
			
		}

	}

}
