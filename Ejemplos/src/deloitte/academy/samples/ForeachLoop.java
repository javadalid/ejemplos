package deloitte.academy.samples;

public class ForeachLoop {

	public static void main(String[] args) {
		int[] marks = { 125, 132, 116, 95, 6 };

		int highest_marks = maximum(marks);
		System.out.println("The highest score is : " + highest_marks);

	}

	/**
	 * Method for each loop
	 * 
	 * @param numbers: array type int
	 * @return value type int
	 */
	public static int maximum(int[] numbers) {
		int maxSoFar = numbers[0];

		/*
		 * for each loop
		 */
		for (int num : numbers) {
			if (num > maxSoFar) {
				maxSoFar = num;
			}

		}
		return maxSoFar;
	}

}
