package deloitte.academy.samples;

public enum Mensjaes {
	OK("Ejecucion exitosa."), Error("Ejecucion fallida.");
	
	public String descripcion;
	
	private Mensjaes(String descripcion) {
		this.descripcion=descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
