package deloitte.academy.samples;

public interface Idioma {
	
	public void saludar();
	
	public default String getSaludo(int param1, int param2) {
		return "Saludos";
	}

}
