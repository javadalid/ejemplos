package deloitte.academy.samples;

public class WhileLoop {

	public static void main(String[] args) {
		//expression initialization
		int i=1;
		//test expression
		while(i < 6) {
			System.out.println("Hello World");
			//update expression
			i++;
		}

	}

}
