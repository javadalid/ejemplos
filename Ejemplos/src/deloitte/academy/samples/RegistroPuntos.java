package deloitte.academy.samples;
/**
 * Permite registrar puntos cada que se hace una compra
 * @author jadalid
 *
 */
public interface RegistroPuntos {
	
	public String registrarPunto(int numVenta, String nombre);

}
