package deloitte.academy.samples;

public class Cliente extends Cafeteria implements RegistroPuntos {

	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cliente(int idVenta, double importe) {
		super(idVenta, importe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		return this.getImporte();
	}

	@Override
	public String registrarPunto(int numVenta, String nombre) {
		String mensaje = "";
		int contador = 0;
		
		for (PuntosCafeteria p : EjecutarCafeteria.puntosCafeteria) {
			if (p.getNombre().equals(nombre)) {
				contador += p.getTotalPuntos();
			}
		}
		if(contador==0) {
			// Registra el punto con el tipo de persona
			PuntosCafeteria nuevoObjeto= new PuntosCafeteria(numVenta, 100, "cliente", nombre);
			EjecutarCafeteria.puntosCafeteria.add(nuevoObjeto);
		}
		else {
			// Actualiza la informacion con los nuevos puntos
			
			for(PuntosCafeteria elemento: EjecutarCafeteria.puntosCafeteria) {
				if(elemento.getNombre().equals(nombre)) {
					elemento.setTotalPuntos(contador +10);
				}
			}
		}

		return mensaje;
	}

}
