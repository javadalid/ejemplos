package deloitte.academy.samples;

import java.util.ArrayList;

public class RegistrarPersonas {

	public Persona registrar(Persona persona) {
		Persona objPersona = new Persona();
		objPersona.setNombre(persona.getNombre());
		objPersona.setApellido(persona.getApellido());
		return objPersona;
	}
	
	public static int totalPersona(ArrayList<Persona>persona) {
		int res=persona.size();
		return res;
	}
}
