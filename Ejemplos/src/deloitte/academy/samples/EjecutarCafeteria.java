package deloitte.academy.samples;

import java.util.ArrayList;

public class EjecutarCafeteria {

	public static final ArrayList<PuntosCafeteria> puntosCafeteria = new ArrayList<PuntosCafeteria>();

	public static void main(String[] args) {
		
		
		Cliente cliente = new Cliente(2, 100);
		System.out.println("Total a pagar: " + cliente.cobrar());

		Trabajdor trabajador = new Trabajdor(1, 100);
		System.out.println("Total a pagar: " + trabajador.cobrar());

		ejecutarPago(trabajador);
		ejecutarPago(cliente);
		
		cliente.registrarPunto(1, "E");

	}

	public static void ejecutarPago(Cafeteria cafeteria) {
		System.out.println("Total a pagar: " + cafeteria.cobrar());
	}

}
