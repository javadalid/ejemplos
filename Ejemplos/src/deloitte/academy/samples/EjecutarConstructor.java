package deloitte.academy.samples;

public class EjecutarConstructor {

	public static void main(String[] args) {
		Constructor constructor = new Constructor();
		System.out.println(constructor.getId() + " "+ constructor.getNombre());
		Constructor constructor2= new Constructor (3, "yo mero");
		Constructor constructor3= new Constructor (3);

		System.out.println("Suma :"+ constructor3.getId() );
	}

}
